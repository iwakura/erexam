-module(twofold).
-export([twofold/4]).

-spec twofold(fun((A,B,R) -> R), R, [A], [B]) -> R.
twofold(_F, Z, [], _Bs) ->
  Z;
twofold(_F, Z, _As, []) ->
  Z;
twofold(F, Z, [A|As], [B|Bs]) ->
  F(A, B, twofold(F, Z, As, Bs)).


