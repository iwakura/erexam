-module(sequence).
-export([fromRule/1, add/2, scaleBy/2, nth/2]).
-export_type([sequence/0]).

% sequences are represented by tuples whose second element is a function
-type sequence() :: {seq, fun((non_neg_integer()) -> float())}.

-spec fromRule(fun((non_neg_integer()) -> float())) -> sequence().
fromRule(F) ->
  {seq, F}.

-spec add(sequence(), sequence()) -> sequence().
add({seq, F1}, {seq, F2}) ->
  {seq, fun(X) -> F1(X) + F2(X) end}.

-spec scaleBy(sequence(), float()) -> sequence().
scaleBy({seq, F}, X) ->
  {seq, fun(A) -> X * F(A) end}.

-spec nth(sequence(), non_neg_integer()) -> float().
nth({seq, F}, N) ->
  F(N).

