% $Id: sequence_tests.erl,v 1.1 2013/04/21 16:12:50 leavens Exp leavens $
-module(sequence_tests).
-import(sequence,[fromRule/1, add/2, scaleBy/2, nth/2]).
-import(testing,[dotests/2,eqTest/3]).
-export([main/0]).
main() -> dotests("sequence_tests $Revision: 1.1 $", tests()).
tests() ->
  Ones = fromRule(fun(_N) -> 1.0 end),
  Squares = fromRule(fun(N) -> N*N end),
  Sp1 = add(Ones, Squares),
  [eqTest(nth(Ones, 0), "==", 1.0),
    eqTest(nth(Ones, 63323), "==", 1.0),
    eqTest(nth(Ones, 99999999999999999999999999999999999999), "==", 1.0),
    eqTest(nth(Squares, 0), "==", 0.0),
    eqTest(nth(Squares, 1), "==", 1.0),
    eqTest(nth(Squares, 2), "==", 4.0),
    eqTest(nth(Squares, 10), "==", 100.0),
    eqTest(nth(Squares, 1000), "==", 1000000.0),
    eqTest(nth(Sp1, 0), "==", 1.0),
    eqTest(nth(Sp1, 1), "==", 2.0),
    eqTest(nth(Sp1, 2), "==", 5.0),
    eqTest(nth(Sp1, 1000), "==", 1000001.0),
    eqTest(nth(scaleBy(Squares,3.0), 1), "==", 3.0),
    eqTest(nth(scaleBy(Squares,3.0), 5), "==", 75.0),
    eqTest(nth(scaleBy(Squares,3.0), 4020), "==", 4020.0*4020.0*3.0),
    eqTest(nth(add(fromRule(fun(N) -> 3.0*N*N + 2.0*N + 1.0 end),
          fromRule(fun(N) -> 40.20*N*N*N + 6.0*N*N end)),
        10),
      "==", 41121.0),
    eqTest(nth(add(fromRule(fun(N) -> 3.0*N*N + 2.0*N + 1.0 end),
          fromRule(fun(N) -> 40.20*N*N*N + 6.0*N*N end)),
        100),
      "==", 40290201.00000001) % some imprecision in this result
  ].
