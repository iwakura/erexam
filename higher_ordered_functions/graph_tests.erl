% $Id: graph_tests.erl,v 1.1 2013/04/21 16:12:50 leavens Exp leavens $
-module(graph_tests).
-import(graph,[graph/2]).
-import(testing,[dotests/2,eqTest/3]).
-export([main/0]).
main() -> dotests("graph_tests $Revision: 1.1 $", tests()).
tests() ->
  [eqTest(graph(fun(X) -> X*42 end, []),"==",[]),
    eqTest(graph(fun(X) -> X+1 end, [1,2,3]),"==",[{1,2}, {2,3}, {3,4}]),
    eqTest(graph(fun(X) -> (3*X)+1 end, [7,9,10,27]),
      "==", [{7,22}, {9,28}, {10,31}, {27,82}]),
    eqTest(graph(fun length/1, [[], "abc", "Erlang"]),
      "==", [{[],0}, {"abc", 3}, {"Erlang", 6}])
  ].
