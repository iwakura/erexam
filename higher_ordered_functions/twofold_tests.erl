-module(twofold_tests).
-import(testing,[dotests/2,eqTest/3]).
-import(twofold,[twofold/4]).
-export([main/0]).
main() -> dotests("twofold_tests $Revision: 1.1 $", tests()).
% 3 functions defined just for testing purposes, not for you to implement
sumtwo(As, Bs) -> twofold(fun(A,B,R) -> A+B+R end, 0, As, Bs).
prodtwo(As, Bs) -> twofold(fun(A,B,R) -> A*B*R end, 1, As, Bs).
zip(As, Bs) -> twofold(fun(A,B,R) -> [{A,B}|R] end, [], As, Bs).
tests() ->
  [eqTest(sumtwo([],[]),"==",0),
    eqTest(sumtwo([5,7,10],[]),"==",0),
    eqTest(sumtwo([],[5,7,10,11]),"==",0),
    eqTest(sumtwo([1,2,3],[10,20,30,40]),"==",11+22+33),
    eqTest(prodtwo([1,2,3],[10,20,30,40]),"==",10*2*20*3*30),
    eqTest(zip([2.73,3.14],[]),"==",[]),
    eqTest(zip([],[1,2,3]),"==",[]),
    eqTest(zip([5,7,4],[1,2,3]),"==",[{5,1}, {7,2}, {4,3}]),
    eqTest(zip([a,b,c,d],[1,2,3,4]),"==",[{a,1}, {b,2}, {c,3}, {d,4}])
].
