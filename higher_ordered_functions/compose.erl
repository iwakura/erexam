-module(compose).
-export([compose/2]).

-spec compose(fun((B) -> C), fun((A) -> B)) -> fun((A) -> C).
compose(Fun1, Fun2) ->
  fun(Arg) -> Fun1(Fun2(Arg)) end.

