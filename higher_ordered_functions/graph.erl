-module(graph).
-export([graph/2]).

-spec graph(fun((A) -> B), [A]) -> [{A,B}].
graph(F, Xs) ->
  lists:foldl(fun(X, Acc) -> [{X, F(X)} | Acc] end, [], lists:reverse(Xs)).

