-module(censor).
-export([censor/2]).

-spec censor([[atom()]], [atom()]) -> [[atom()]].
censor(Document, BadWords) when is_list(Document) and is_list(BadWords) ->
  [censor_para(Para, BadWords) || Para <- Document].

-spec censor_para([atom()], [atom()]) -> [atom()].
censor_para(Para, BadWords) when is_list(Para) and is_list(BadWords) ->
  [Word || Word <- Para, not lists:member(Word, BadWords)].

