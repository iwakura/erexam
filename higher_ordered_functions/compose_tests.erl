% $Id: compose_tests.erl,v 1.1 2013/04/21 16:12:50 leavens Exp leavens $
-module(compose_tests).
-import(compose,[compose/2]).
-import(testing,[dotests/2,eqTest/3]).
-export([main/0]).
main() -> dotests("compose_tests $Revision: 1.1 $", tests()).
tests() ->
  [eqTest((compose(fun(X) -> X+1 end, fun(Y) -> Y*3 end))(7),"==",22),
    eqTest((compose(fun(X) -> X==21 end, fun(Y) -> Y*3 end))(7),"==",true),
    eqTest((compose(fun(Y) -> Y*3 end, fun(X) -> X+1 end))(7),"==",24),
    eqTest((compose(fun(B) -> not B end, fun(Ls) -> Ls == [] end))([]),"==",false),
    eqTest((compose(fun(N) -> {N*2} end, fun(N) -> N+5 end))(6),"==",{22}),
    eqTest((compose(fun(X) -> {X,2} end, fun(Y) -> [Y,1] end))(6),"==",{[6,1],2})
  ].
