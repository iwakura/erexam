-module(compositor).
-export([start/0, loop/1]).

start() ->
  spawn(?MODULE, loop, [fun(Arg) -> Arg end]).

loop(Fun) ->
  receive
    {Pid, apply_to, Value} ->
      Result = Fun(Value),
      Pid ! {self(), value_is, Result},
      MyFun = Fun;
    {Pid, compose_with, NewFun} ->
      Pid ! {self(), composed},
      MyFun = fun(Arg) -> NewFun(Fun(Arg)) end;
    {Pid, start_over_with, NewFun} ->
      Pid ! {self(), started_over},
      MyFun = NewFun
  end,
  loop(MyFun).


