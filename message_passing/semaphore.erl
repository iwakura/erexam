-module(semaphore).
-export([start/1, loop/3]).

start(N) when is_integer(N), N > 0 ->
  spawn(?MODULE, loop, [N, [], []]).

loop(N, Leasers, Waiters) ->
  receive
    {Pid, lock} ->
      case N > 0 of
        true ->
          NewN = N - 1,
          Pid ! {self(), go_ahead},
          NewLeasers = [Pid | Leasers],
          NewWaiters = Waiters;
        false ->
          NewWaiters = [Pid | Waiters],
          NewLeasers = Leasers,
          NewN = N
      end;
    {Pid, done} ->
      Pid ! {self(), thanks},
      case length(Waiters) of
        0 ->
          NewWaiters = [],
          NewLeasers = lists:delete(Pid, Leasers),
          NewN = N + 1;
        _N ->
          [Waiter | T] = lists:reverse(Waiters),
          Waiter ! {self(), go_ahead},
          NewLeasers = [Waiter | lists:delete(Pid, Leasers)],
          NewWaiters = lists:reverse(T),
          NewN = N
      end
  end,
  loop(NewN, NewLeasers, NewWaiters).

