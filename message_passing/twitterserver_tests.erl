% $Id: twitterserver_tests.erl,v 1.2 2013/04/24 21:51:28 leavens Exp leavens $
-module(twitterserver_tests).
-import(twitterserver,[start/0, tweet/2, fetch/2]).
-import(testing,[dotests/2,eqTest/3]).
-export([main/0]).
main() -> dotests("twitterserver_tests $Revision: 1.2 $", tests()).
tests() ->
  TS = twitterserver:start(),
  [eqTest(tweet(TS, "Starting to use twitter"), "==", tweeted),
    eqTest(tweet(TS, "Where's the send button?"), "==", tweeted),
    eqTest(fetch(TS, 1), "==", ["Where's the send button?"]),
    eqTest(fetch(TS, 2), "==", ["Where's the send button?", "Starting to use twitter"]),
    eqTest(tweet(TS, "Chirp!"), "==", tweeted),
    eqTest(tweet(TS, "Caw! Caw!"), "==", tweeted),
    eqTest(tweet(TS, "Look out for that cat!"), "==", tweeted),
    eqTest(fetch(TS, 0), "==", []),
    eqTest(fetch(TS, 1), "==", ["Look out for that cat!"]),
    eqTest(fetch(TS, 5), "==", ["Look out for that cat!", "Caw! Caw!", "Chirp!",
        "Where's the send button?", "Starting to use twitter"]),
    eqTest(fetch(TS, 10), "==", ["Look out for that cat!", "Caw! Caw!", "Chirp!",
        "Where's the send button?", "Starting to use twitter"]),
    eqTest(tweet(TS, "My poor birdie!"), "==", tweeted),
    eqTest(tweet(TS, "It didn't see dat putythat!"), "==", tweeted),
    eqTest(fetch(TS, 3), "==", ["It didn't see dat putythat!",
        "My poor birdie!",
        "Look out for that cat!"])
  ].
