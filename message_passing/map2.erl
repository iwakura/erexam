-module(map2).
-export([map2/3, reverse/1]).


-spec map2(fun((A,B) ->C), [A], [B]) -> [C].
map2(F, As, Bs) ->
  reverse(map2(F, As, Bs, [])).

map2(_F, [], _Bs, Acc) ->
  Acc;
map2(_F, _As, [], Acc) ->
  Acc;
map2(F, [Ah| At], [Bh | Bt], Acc) ->
  map2(F, At, Bt, [F(Ah, Bh) | Acc]).

reverse(List) ->
  reverse(List, []).

reverse([], Acc) ->
  Acc;
reverse([H | T], Acc) ->
  reverse(T, [H | Acc]).
