% $Id: compositor_tests.erl,v 1.2 2013/04/24 21:51:28 leavens Exp leavens $
-module(compositor_tests).
-import(testing,[dotests/2,eqTest/3]).
-export([main/0, applyTo/2, composeWith/2, startOver/2]).
main() -> dotests("compositor_tests $Revision: 1.2 $", tests()).
tests() ->
  Comp = compositor:start(),
  [eqTest(applyTo(Comp, 42), "==", 42),
    eqTest(applyTo(Comp, atom), "==", atom),
    eqTest(composeWith(Comp, fun(X) -> 3*X*X end), "==", ok),
    eqTest(applyTo(Comp, 10), "==", 300),
    eqTest(applyTo(Comp, 11), "==", 363),
    eqTest(composeWith(Comp, fun(Y) -> 5+Y end), "==", ok),
    eqTest(applyTo(Comp, 10), "==", 305),
    eqTest(applyTo(Comp, 11), "==", 368),
    eqTest(startOver(Comp, fun(X) -> X == turkey end), "==", ok),
    eqTest(applyTo(Comp, turkey), "==", true),
    eqTest(applyTo(Comp, cheese), "==", false)
  ].
% helpers for testing (client functions), NOT for you to implement
applyTo(Comp, Value) ->
  Comp ! {self(), apply_to, Value},
  receive {Comp, value_is, Result} -> Result end.
composeWith(Comp, NewFun) ->
  Comp ! {self(), compose_with, NewFun},
  receive {Comp, composed} -> ok end.
startOver(Comp, NewFun) ->
  Comp ! {self(), start_over_with, NewFun},
  receive {Comp, started_over} -> ok end.
