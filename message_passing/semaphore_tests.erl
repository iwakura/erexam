% $Id: semaphore_tests.erl,v 1.1 2013/04/21 22:15:13 leavens Exp $
-module(semaphore_tests).
-import(semaphore,[start/1]).
-import(testing,[dotests/2,eqTest/3]).
-export([main/0,clientstart/2,statusloop/3]).
main() -> dotests("semaphore_tests $Revision: 1.1 $", tests()).
tests() ->
  S1 = semaphore:start(1),
  S2 = semaphore:start(2),
  Dijkstra = startclient(S1),
  Hoare = startclient(S2),
  BrinchHansen = startclient(S2),
  Liskov = startclient(S1),
  [eqTest(status(Dijkstra), "==", locked),
    eqTest(status(Liskov), "==", waiting),
    eqTest(finish(Dijkstra), "==", sent_done),
    eqTest(status(Dijkstra), "==", done),
    eqTest(status(Liskov), "==", locked),
    eqTest(status(Hoare), "==", locked),
    eqTest(status(BrinchHansen), "==", locked),
    eqTest(finish(BrinchHansen), "==", sent_done),
    eqTest(status(BrinchHansen), "==", done),
    eqTest(finish(Hoare), "==", sent_done),
    eqTest(status(Hoare), "==", done)
  ].
% helpers for testing (client functions), NOT for you to implement
startclient(Semaphore) ->
  Pid = clientaction(Semaphore),
  receive {Pid, sent_lock} -> Pid end.
clientaction(Semaphore) -> spawn(?MODULE,clientstart,[Semaphore,self()]).
clientstart(Semaphore,TestPid) ->
  Semaphore ! {self(), lock},
  TestPid ! {self(), sent_lock},
  statusloop(Semaphore,TestPid,waiting).
statusloop(Semaphore,TestPid,Status) ->
  receive
    {Semaphore, go_ahead} -> statusloop(Semaphore, TestPid, locked);
    {Semaphore, thanks} -> statusloop(Semaphore, TestPid, done);
    {TestPid, status} -> TestPid ! {self(), status_is, Status},
      statusloop(Semaphore, TestPid, Status);
    {TestPid, finish} when Status == locked ->
      Semaphore ! {self(), done},
      TestPid ! {self(), sent_done},
      statusloop(Semaphore, TestPid, Status)
  end.
status(Client) ->
  Client ! {self(), status},
  receive {Client, status_is, Status} -> Status end.
finish(Client) ->
  Client ! {self(), finish},
  receive {Client, sent_done} -> sent_done end.
