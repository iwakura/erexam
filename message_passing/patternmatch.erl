-module(patternmatch).
-export([patternmatch/1, a/0, b/0, c/0, d/0]).

patternmatch(lst) -> 1;
patternmatch(nil) -> 2;
patternmatch([_Lst]) -> 3;
patternmatch([[_Lst]]) -> 6;
patternmatch([x|xs]) -> 4;
patternmatch([_X|_Xs]) -> 5;
patternmatch(_Number) -> 7.
% Functions for each of the parts of this question:
a() -> patternmatch([atoms, are, here]).
b() -> patternmatch(an_atom).
c() -> patternmatch(lst).
d() -> patternmatch([[some, nested], [lists]]).
