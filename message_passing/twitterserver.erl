-module(twitterserver).
-export([start/0, loop/1, tweet/2, fetch/2]).

-spec start() -> pid().
start() ->
  spawn(?MODULE, loop, [[]]).

loop(History) ->
  receive
    {Pid, tweet, Tweet} ->
      NewHistory = [Tweet | History],
      Pid ! tweeted;
    {Pid, fetch, Num} ->
      Tweets = lists:sublist(History, Num),
      Pid ! {tweets, Tweets},
      NewHistory = History
  end,
  loop(NewHistory).


-spec tweet(pid(), string()) -> tweeted.
tweet(Pid, Tweet) when is_pid(Pid) ->
  Pid ! {self(), tweet, Tweet},
  receive
    tweeted ->
      tweeted
  end.

-spec fetch(pid(), non_neg_integer()) -> [string()].
fetch(Pid, Num) when is_pid(Pid), is_integer(Num), Num >= 0 ->
  Pid ! {self(), fetch, Num},
  receive
    {tweets, Tweets} ->
      Tweets
  end.


