% $Id: map2_tests.erl,v 1.1 2013/04/21 22:15:13 leavens Exp $
-module(map2_tests).
-import(map2,[map2/3]).
-import(testing,[dotests/2,eqTest/3]).
-export([main/0]).
main() -> dotests("map2_tests $Revision: 1.1 $", tests()).
tests() ->
  [eqTest(map2(fun(A,B) -> A+B end, [], []),"==",[]),
    eqTest(map2(fun(_A,_B) -> 3 end, [1,2,3], []),"==",[]),
    eqTest(map2(fun(A,B) -> A*B end, [], [1,2,3]),"==",[]),
    eqTest(map2(fun(A,B) -> A+B end, [4,5,6], [1,2,3]),"==",[4+1,5+2,6+3]),
    eqTest(map2(fun(A,B) -> {A,B} end, [a,b,c,d,e], [5,9,2,4,42]),
      "==", [{a,5}, {b,9}, {c,2}, {d,4}, {e,42}]),
    eqTest(map2(fun(X,Y) -> X == Y end, [x,y,z,z,y,foo],[y,x,a,z,q,foo]),
      "==", [false, false,false,true,false,true])
  ].
